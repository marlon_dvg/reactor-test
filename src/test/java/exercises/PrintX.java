package exercises;

public class PrintX {
    static int n = 0;

    public static void main(String[] args) {
        if (n == 0) {
            System.out.println("ERROR");
        } else {

            final String X = "X";
            final String UNDERSCORE = "_";
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j || i == (n - 1 - j)) {
                        result.append(X);
                    } else {
                        result.append(UNDERSCORE);
                    }
                }
                result.append("\n");
            }

            System.out.println(result);
        }
    }
}
