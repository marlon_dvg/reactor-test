package exercises;

public class OrdenarBloqyes {
    static Integer myArray[] = {1, 3, 2, 0, 7, 8, 1, 3, 0, 6, 7, 1};
    static int n = 12;

    public static void main(String[] args) {

        Integer orderedArray[] = new Integer[n];
        int min;
        int max;
        int current;
        int next;

        int count = 0;

        for (int i = 0; i < n; i++) {
            current = myArray[i];
            next = i+1 == n ? -1 : myArray[i+1];

            if (i == 0) {
                min = current;
            } else {
                if(next != -1){
                    min = Math.min(current, next);
                }
            }
        }
    }
}
